#!/bin/bash
# vim: dict=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /CoreOS/chkconfig/Regression/chkconfig-prioritory-set-inconsistently
#   Description: chkconfig-prioritory-set-inconsistently
#   Author: Martin Cermak <mcermak@redhat.com>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2012 Red Hat, Inc. All rights reserved.
#
#   This copyrighted material is made available to anyone wishing
#   to use, modify, copy, or redistribute it subject to the terms
#   and conditions of the GNU General Public License version 2.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE. See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public
#   License along with this program; if not, write to the Free
#   Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
#   Boston, MA 02110-1301, USA.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/lib/beakerlib/beakerlib.sh

PACKAGE="chkconfig"

myGetPriority() {
        SERVICE=$1
        basename $( ls /etc/rc.d/rc3.d/S[0-9][0-9]$SERVICE ) | tr -d "[a-zA-Z\-]"
}

# Test scenario: 
# https://bugzilla.redhat.com/show_bug.cgi?id=771455#c0

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "cp service-{a,b,c}-fake /etc/rc.d/init.d/"

        # A has Required-Start on B or C, thus the reversed order:
        rlRun "/usr/lib/lsb/install_initd service-c-fake"
        rlRun "/usr/lib/lsb/install_initd service-b-fake"
        rlRun "/usr/lib/lsb/install_initd service-a-fake"

        rlRun "chkconfig service-a-fake off"
        rlRun "chkconfig service-b-fake off"
        rlRun "chkconfig service-c-fake off"
    rlPhaseEnd

    rlPhaseStartTest
        rlRun "chkconfig service-b-fake on"
        rlRun "chkconfig service-a-fake on"
        rlRun "PRIO=$( myGetPriority service-a-fake )"
        # now, the priority of A should be >= 40 (should be = 40 in ideal world)
        # and hopefully it is < 80
        rlRun "[ $PRIO -ge 40 ]"
        rlRun "[ $PRIO -lt 80 ]"

        rlRun "chkconfig service-c-fake on"
        rlRun "PRIO=$( myGetPriority service-a-fake )"
        # MAIN TEST:
        # right after enabling C, A should raise its priority above 80
        # this should fail with unpatched package (e.g. chkconfig-1.3.47-1.el6)
        # ... and pass with patched one (e.g. chkconfig-1.3.49.3-2.el6)
        rlRun "[ $PRIO -gt 80 ]"
    rlPhaseEnd

    rlPhaseStartCleanup
        rlRun "/usr/lib/lsb/remove_initd service-a-fake"
        rlRun "/usr/lib/lsb/remove_initd service-b-fake"
        rlRun "/usr/lib/lsb/remove_initd service-c-fake"
        rlRun "rm -rf /etc/rc.d/init.d/service-{a,b,c}-fake"
    rlPhaseEnd
#rlJournalPrintText
rlJournalEnd
